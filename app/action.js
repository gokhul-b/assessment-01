"use server";
export const storeFormData = (form) => {
  console.log(form);
  try {
    const fs = require("fs");
    fs.appendFileSync("form_data.txt", JSON.stringify(form) + "\n");
    return "Form Submitted";
  } catch {
    return "Form is not submitted";
  }
};
