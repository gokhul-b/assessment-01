"use client";
import { useState } from "react";
import { storeFormData } from "./action";

export default function Home() {
  const [form, setForm] = useState({
    name: "",
    age: "",
    city: "",
    pincode: "",
  });
  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await storeFormData(form);
      console.log(response);
    } catch (error) {
      console.error("Error submitting form data:", error);
    }
  };
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <div>
        <p className="text-center mb-5 text-2xl">Register Form</p>
        <form
          onSubmit={handleSubmit}
          className="flex flex-col space-y-5 text-black"
        >
          <input
            type="text"
            name="name"
            onChange={handleChange}
            placeholder="Name"
            className="px-2 py-2 rounded-lg"
          />
          <input
            type="text"
            name="age"
            onChange={handleChange}
            placeholder="Age"
            className="px-2 py-2 rounded-lg"
          />
          <input
            type="text"
            name="city"
            onChange={handleChange}
            placeholder="City"
            className="px-2 py-2 rounded-lg"
          />
          <input
            type="text"
            name="pincode"
            onChange={handleChange}
            placeholder="Pincode"
            className="px-2 py-2 rounded-lg"
          />
          <button type="submit" className="text-white px-2 py-1 bg-green-400">
            Submit
          </button>
        </form>
      </div>
    </main>
  );
}
